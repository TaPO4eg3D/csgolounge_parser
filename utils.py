import configparser


class Config:

    def __init__(self):
        self.config = configparser.ConfigParser()
        self.config.read('config.ini')

        self.min_trades = int(self.config['MAIN']['Trades'])
        self.min_messages = int(self.config['MAIN']['Messages'])
        self.min_reputation = int(self.config['MAIN']['Reputation'])
        self.steam_lvl = int(self.config['MAIN']['SteamLVL'])

        self.lounge_login = self.config['LOUNGE']['Login']
        self.lounge_password = self.config['LOUNGE']['Pass']

        self.steam_login = self.config['STEAM']['Login']
        self.steam_password = self.config['STEAM']['Pass']
        self.steam_API = self.config['STEAM']['API']
        self.trade_message = self.config['STEAM']['TradeMessage']
