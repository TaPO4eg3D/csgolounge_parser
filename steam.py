import json
from steampy.client import SteamClient, Asset
from steampy.utils import GameOptions


class WorkWithSteam:

    def __init__(self, login: str, password: str, api_key: str):
        self.steam_client = SteamClient(api_key)
        self.steam_client.login(login, password, 'SteamGuard.txt')
        is_session_alive = self.steam_client.is_session_alive()
        if is_session_alive:
            pass
        else:
            print('Unable to establish connection, check a correctness of login and password')
            exit(0)

    def send_trade_offer(self, partner_id, trade_offer_url, items, lounge_url, message, game=GameOptions.CS):
        trades_log_file = open('trades_log.log', 'ab')
        trades_log_message = ''
        is_session_alive = self.steam_client.is_session_alive()
        if is_session_alive:
            pass
        else:
            print('Unable to establish connection')

        try:
            partner_items = self.steam_client.get_partner_inventory(partner_id, game)
        except AttributeError:
            trades_log_message += '{}: Инвентарь закрыт или пуст\n'.format(lounge_url)
            return

        assets = list()
        trades_log_message += '{} предметы: \n'.format(lounge_url)
        for item in items:
            for i in partner_items.values():
                if i['market_hash_name'] == item:
                    assets.append(Asset(i['id'], game))
                    trades_log_message += '{} \n'.format(i['market_hash_name'])
                    break

        if not assets:
            trades_log_message += '{} \n'.format('запрашиваемые предметы не найдены')
            return

        message = message.replace('"', '')
        response = self.steam_client.make_offer_with_url([], assets, trade_offer_url, message=message)
        trades_log_message += '{} ответ: \n{}\n\n\n\n'.format(lounge_url, json.dumps(response))
        trades_log_file.write(trades_log_message.encode())
        trades_log_file.close()

    def get_all_trades(self):
        response = self.steam_client.get_trade_offers()['response']['trade_offers_sent']
        return response

    def cancel_offer(self, tradeofferid):
        self.steam_client.cancel_trade_offer(tradeofferid)




