import os
import re
import time
import pickle
import sqlite3
import requests
import threading
from collections import deque
from bs4 import BeautifulSoup
from asciimatics.screen import Screen
from selenium import webdriver
from utils import Config
from steam import WorkWithSteam
from datetime import datetime


class LoungeBot:

    # Global counters
    trades_counter = 0
    matched_trades_counter = 0
    offers_sent = 0

    # Inner counters
    inner_trades_counter = 0
    inner_matched_trades_counter = 0
    inner_offers_sent = 0

    trade_links = list()  # trade link storage
    matched_trade_links = list()  # matched trade link storage

    def __init__(self, screen, config):
        self.screen = screen  # Main screen object
        self.config = config
        self.conn = None  # Database connection
        self.c = None  # Database cursor
        self.session = None  # Current session on csgolounge.com
        self.steam = WorkWithSteam(config.steam_login, config.steam_password, config.steam_API)

    def display(self):  # Main screen window
        self.screen.print_at('Gathered links: {}'.format(self.trades_counter), 0, 0, Screen.COLOUR_GREEN)
        self.screen.print_at('Relevant conditions: {}'.format(self.matched_trades_counter), 0, 1, Screen.COLOUR_GREEN)
        self.screen.print_at('Sent offers: {}'.format(self.offers_sent), 0, 3, Screen.COLOUR_GREEN)
        self.screen.refresh()

    def connect_to_database(self):  # Connection to the database
        # Remove temp file if exists
        try:
            os.remove('trades.db')
        except OSError:
            pass

        self.conn = sqlite3.connect('trades.db')
        self.c = self.conn.cursor()

        self.c.execute('''
            CREATE TABLE trades
            (
                trade_id integer primary key autoincrement,
                link text NOT NULL
            )
        ''')
        self.conn.commit()

    def selenium_auth(self):  # Authentication in browser
        self.screen.print_at('Loading browser for auth...', 0, 0)
        self.screen.refresh()
        driver = webdriver.Firefox(executable_path='geckodriver.exe')
        driver.implicitly_wait(10)
        driver.get('https://csgolounge.com/')
        driver.find_element_by_xpath('//*[@id="status"]/a[2]').click()
        driver.find_element_by_xpath('//*[@id="lemail"]').send_keys(self.config.lounge_login)
        driver.find_element_by_xpath('//*[@id="lpassword"]').send_keys(self.config.lounge_password)
        driver.find_element_by_xpath('//*[@id="logIn"]/form/div/div[4]/a').click()
        time.sleep(10)
        cookies = driver.get_cookies()
        driver.close()
        self.screen.clear()

        pickle.dump(cookies, open('cookies.pkl', 'wb'))

        today = datetime.today()

        pickle.dump(today, open('cookies_lifetime.pkl', 'wb'))

        return cookies

    def authentication(self):  # Main auth method

        if os.path.exists('cookies.pkl'):
            self.screen.print_at('Load cookies from file...', 0, 0)
            self.screen.refresh()
            cookies_lifetime = pickle.load(open('cookies_lifetime.pkl', 'rb'))
            today = datetime.today()
            if (today - cookies_lifetime).days > 5:
                self.screen.print_at('Stale cookies, load browser...', 0, 0)
                self.screen.refresh()
                cookies = self.selenium_auth()
            else:
                cookies = pickle.load(open('cookies.pkl', 'rb'))
                time.sleep(2)
                self.screen.clear()
        else:
            cookies = self.selenium_auth()
        self.session = requests.Session()
        for cookie in cookies:
            self.session.cookies.set(cookie['name'], cookie['value'])

    def get_trades(self):  # Parsing all possible trades
        while True:
            for i in range(2):
                t = threading.Thread(target=self.trades_request)
                t.start()
                t.join()
            for trade_link in self.trade_links:
                self.trades_counter += 1
                self.inner_trades_counter += 1
                self.c.execute('''
                    INSERT INTO trades(link)
                    VALUES (
                        ?
                    );
                ''', (trade_link,))
                self.display()
            self.conn.commit()
            self.trade_links.clear()

            if self.inner_trades_counter >= 100:
                self.inner_trades_counter = 0
                urls = self.c.execute('''
                    SELECT DISTINCT link FROM trades;
                ''')
                urls = [url[0] for url in urls]
                self.c.execute('DELETE FROM trades')
                self.conn.commit()
                urls_first = urls[int(len(urls) / 2):]
                urls_second = urls[:int(len(urls) / 2)]
                gf1 = threading.Thread(target=self.get_full_info, args=(urls_first,))
                gf2 = threading.Thread(target=self.get_full_info, args=(urls_second,))
                gf1.start()
                gf2.start()

    def trades_request(self):
        req = self.session.get('https://csgolounge.com/trades')
        html = req.text

        soup = BeautifulSoup(html, 'lxml')
        trades = soup.find_all('div', class_='tradepoll')

        for trade in trades:
            trade_link = trade.find_all('a', href=True)[0]['href']
            trade_link = 'https://csgolounge.com/' + trade_link
            self.trade_links.append(trade_link)

    def get_full_info(self, urls):
        for url in urls:
            html = self.session.get(url).text
            soup = BeautifulSoup(html, 'lxml')
            profile_info = soup.find('div', {'class': 'profilesmallheader'})
            profile_info = str(profile_info).split('<br/>')
            trades = int(re.search(r'\d+', profile_info[1]).group(0))
            messages = int(re.search(r'\d+', profile_info[2]).group(0))
            reputation = int(re.search(r'\S\d+|\d+', profile_info[3]).group(0))
            steam_lvl = int(re.search('\d+', soup.find('div', {'class': 'slvl'}).contents[0]).group(0))
            if trades <= self.config.min_trades and \
                            messages <= self.config.min_messages and \
                            reputation <= self.config.min_reputation and \
                            steam_lvl >= self.config.steam_lvl:
                self.matched_trade_links.append(url)
                self.matched_trades_counter += 1
                self.inner_matched_trades_counter += 1
            else:
                pass

        if self.inner_matched_trades_counter >= 20:
            self.inner_matched_trades_counter = 0
            urls_first = self.matched_trade_links[int(len(urls) / 2):]
            urls_second = self.matched_trade_links[:int(len(urls) / 2)]
            self.matched_trade_links.clear()
            mtp1 = threading.Thread(target=self.matched_trades_post, args=(urls_first,))
            mtp2 = threading.Thread(target=self.matched_trades_post, args=(urls_second,))
            mtp1.start()
            mtp2.start()

    def matched_trades_post(self, urls):
        for url in urls:
            html = self.session.get(url)
            soup = BeautifulSoup(html.text, 'lxml')

            tob = soup.find_all('a', {'class': 'buttonright'})
            steam_id = ''
            trade_offer_link = ''
            for i in tob:
                if i.contents[0] == 'Add on Steam':
                    steam_id = i.get('href', ).split('/')[-1]
                elif i.contents[0] == 'Steam offer':
                    trade_offer_link = i.get('href', )

            if trade_offer_link == '':
                desc_box = soup.find('p', {'class': 'standard msgtxt'})

                rep = {"<br/>": "", "</p>": ""}
                for i, j in rep.items():
                    desc_box = str(desc_box).replace(i, j)

                _urls = re.findall('http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+',
                                   str(desc_box))
                for _url in _urls:
                    if 'tradeoffer' in _url:
                        trade_offer_link = _url
                        break

            if trade_offer_link == '':
                print('Undefined offer link, skipping - ' + url)
                continue

            self.stay_in_range()
            items = self.get_partner_items(url)
            self.steam.send_trade_offer(steam_id, trade_offer_link, items, url, self.config.trade_message)
            self.inner_offers_sent += 1
            self.offers_sent += 1

    def get_partner_items(self, link):
        items = list()
        html = self.session.get(link)
        soup = BeautifulSoup(html.text, 'lxml')
        left_section = soup.find('form', {'class': 'left'})

        for item in left_section.find_all('img', {'class': 'smallimg'}):
            item_name = item.get('alt', )
            if 'Any' in item_name:
                pass
            else:
                items.append(item_name)

        return items

    def stay_in_range(self):
        trades = self.steam.get_all_trades()
        if len(trades) >= 30:
            cancel_trades_log = open('cancel_trades.log', 'a')
            last_trade_id = trades[-1]['tradeofferid']
            self.steam.cancel_offer(last_trade_id)
            cancel_trades_log.write('Предложение с ID {} было отменено(истекло время ожидания)\n\n'.format(last_trade_id))
            cancel_trades_log.close()


def main(screen):
    config = Config()
    lounge_bot = LoungeBot(screen, config)
    lounge_bot.authentication()
    lounge_bot.connect_to_database()
    lounge_bot.get_trades()

Screen.wrapper(main)
